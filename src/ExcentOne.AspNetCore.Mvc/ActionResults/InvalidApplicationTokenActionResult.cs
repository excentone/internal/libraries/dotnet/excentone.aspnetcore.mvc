﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace ExcentOne.AspNetCore.Mvc.ActionResults
{
    [SuppressMessage("ReSharper", "IdentifierTypo")]
    public class InvalidApplicationTokenActionResult : IActionResult
    {
        public static string HTTPHEADER_INVALID_APPLICATION_TOKEN = "Application Token is not valid";
        public static int HTTPCODE_INVALID_APPLICATION_TOKEN = 902;
        

        public async Task ExecuteResultAsync(ActionContext context)
        {
            var objectResult = new ObjectResult(new ArgumentException(HTTPHEADER_INVALID_APPLICATION_TOKEN))
            {
                StatusCode = HTTPCODE_INVALID_APPLICATION_TOKEN
            };

            await objectResult.ExecuteResultAsync(context);
        }
    }
}