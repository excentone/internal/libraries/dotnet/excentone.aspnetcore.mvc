﻿using ExcentOne.AspNetCore.Mvc.ActionResults;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Hosting;

namespace ExcentOne.AspNetCore.Mvc.Extensions.Attributes
{
    public class RequireUserInfoAttribute : BaseActionFilterAttribute
    {
        public RequireUserInfoAttribute(IWebHostEnvironment env)
        {
            Environment = env;
        }

        public IWebHostEnvironment Environment { get; }

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            base.OnActionExecuting(filterContext);
            if (Environment.IsDevelopment()) return;
            if (!IsHttpHeaderValid(filterContext, USER_INFO_HEADER_NAME))
                filterContext.Result = new InvalidUserInfoActionResult();
        }
    }
}