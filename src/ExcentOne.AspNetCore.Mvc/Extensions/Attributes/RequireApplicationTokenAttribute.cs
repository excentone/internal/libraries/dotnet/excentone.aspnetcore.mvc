﻿using ExcentOne.AspNetCore.Mvc.ActionResults;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;

namespace ExcentOne.AspNetCore.Mvc.Extensions.Attributes
{
    public class RequireApplicationTokenAttribute : BaseActionFilterAttribute
    {
        public RequireApplicationTokenAttribute(IConfiguration configuration, IWebHostEnvironment env)
        {
            Configuration = configuration;
            Environment = env;
        }

        public IConfiguration Configuration { get; }
        public IWebHostEnvironment Environment { get; }

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            base.OnActionExecuting(filterContext);
            if (Environment.IsDevelopment()) return;

            if (!IsHttpHeaderValid(filterContext, EXCENTONE_APPLICATION_HEADER_NAME))
            {
                filterContext.Result = new NullApplicationTokenActionResult();
            }
            else
            {
                if (!filterContext.HttpContext.Request.Headers[EXCENTONE_APPLICATION_HEADER_NAME]
                    .Equals(Configuration["ApplicationKey"]))
                    filterContext.Result = new InvalidApplicationTokenActionResult();
            }
        }
    }
}