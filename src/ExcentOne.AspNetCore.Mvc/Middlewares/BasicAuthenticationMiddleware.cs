﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ExcentOne.AspNetCore.Mvc.Middlewares
{
    public class BasicAuthenticationMiddleware
    {
        private readonly RequestDelegate next;

        public BasicAuthenticationMiddleware(RequestDelegate next)
        {
            this.next = next;
        }

        public async Task Invoke(HttpContext context)
        {
            string authHeader = context.Request.Headers["Authorization"];
            if (authHeader != null && authHeader.StartsWith("Basic"))
            {
                //Extract credentials
                string encodedCredentials = authHeader.Substring("Basic ".Length).Trim();
                Encoding encoding = Encoding.Default;
                string credentials = encoding.GetString(Convert.FromBase64String(encodedCredentials));

                var details = credentials.Split(':');

                var appkey = details[0];
                var k2fqn = details[1];

                if (appkey == "test" && k2fqn == "test")
                {
                    await next.Invoke(context);
                }
                else
                {
                    context.Response.StatusCode = 401; //Unauthorized
                    return;
                }
            }
            else
            {
                // no authorization header
                context.Response.StatusCode = 401; //Unauthorized
                return;
            }
        }
    }
}
