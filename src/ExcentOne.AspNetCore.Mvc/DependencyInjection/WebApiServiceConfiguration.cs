﻿using System;
using ExcentOne.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Formatters;
using Microsoft.Extensions.DependencyInjection;

namespace ExcentOne.AspNetCore.Mvc.DependencyInjection
{
    public static class WebApiServiceConfiguration
    {
        public static IServiceCollection AddWebApiServices(
            this IServiceCollection serviceCollection,
            CompatibilityVersion compatibilityVersion = CompatibilityVersion.Latest,
            Action<MvcOptions> configureMvcOptions = null,
            Action<IMvcCoreBuilder> mvcCoreBuilderAction = null)
        {
            var builder = serviceCollection
                .AddMvcCore(options =>
                {
                    options.OutputFormatters.RemoveType<HttpNoContentOutputFormatter>();
                    options.OutputFormatters.Insert(0, new HttpNoContentOutputFormatter
                    {
                        TreatNullValueAsNoContent = false
                    });
                    options.Filters.Add<NotFoundResultFilter>();
                    configureMvcOptions?.Invoke(options);
                })
                .AddApiExplorer()
                .AddJsonOptions(options => { options.JsonSerializerOptions.WriteIndented = true; })
                .SetCompatibilityVersion(compatibilityVersion);

            mvcCoreBuilderAction?.Invoke(builder);

            return serviceCollection;
        }
    }
}